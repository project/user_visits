<?php

namespace Drupal\user_visits\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure User visits settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_visits_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'user_visits.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_visits.settings');
    $form['user_activity'] = [
      '#markup' => $this->t("There are various Views based blocks available that you might want to configure at the <a href='@href'>blocks administration page</a> to be displayed. Available blocks are <em>My visitors</em>, <em>My visits</em>, <em>User's visitors</em> and <em>User's visits</em>.", ['@href' => Url::fromRoute('block.admin_display')->toString()]),
    ];
    $options = [];
    $roles = user_roles(TRUE);
    foreach ($roles as $key => $label) {
      $options[$key] = $label->get('label');
    }
    $form['hidden_roles'] = [
      '#title' => $this->t('Hidden Roles'),
      '#description' => $this->t('Visits of users with a hidden role are not being tracked.'),
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $config->get('hidden_roles'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('user_visits.settings')
      ->set('hidden_roles', $form_state->getValue('hidden_roles'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
