<?php

namespace Drupal\user_visits\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the user visit entity edit forms.
 */
class UserVisitForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New user visit %label has been created.', $message_arguments));
        $this->logger('user_visits')->notice('Created new user visit %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The user visit %label has been updated.', $message_arguments));
        $this->logger('user_visits')->notice('Updated user visit %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.user_visit.collection');

    return $result;
  }

}
