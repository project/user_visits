<?php

namespace Drupal\user_visits\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user_visits\Entity\UserVisit;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * User visits event subscriber.
 */
class UserVisitsSubscriber implements EventSubscriberInterface {

  /**
   * The config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  private $requestStack;

  /**
   * Constructs an UserVisitsSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      AccountInterface $account,
      RouteMatchInterface $route_match,
      CurrentPathStack $current_path,
      RequestStack $request_stack,
  ) {
    $this->config = $config_factory->get('user_visits.settings');
    $this->account = $account;
    $this->routeMatch = $route_match;
    $this->currentPath = $current_path;
    $this->requestStack = $request_stack;
  }

  /**
   * Kernel response event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Response event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onKernelResponse(ResponseEvent $event) {
    $user_roles = array_intersect(
      $this->account->getRoles(),
      $this->config->get('hidden_roles')
    );

    /** @var \Drupal\user\Entity\User $user */
    $user = \Drupal::routeMatch()->getParameter('user');

    if (
      $this->account->id()
      && empty($user_roles)
      && $this->routeMatch->getRouteName() === 'entity.user.canonical'
      && $this->account->hasPermission('access user profiles')
      && $user->id() !== $this->account->id()
    ) {

      $user_visits = \Drupal::entityTypeManager()->getStorage('user_visit')
        ->loadByProperties([
          'uid' => $user->id(),
          'vuid' => $this->account->id(),
        ]);

      /** @var \Drupal\user_visits\Entity\UserVisit $user_visit */
      if ($user_visit = reset($user_visits)) {
        // Update the changed timestamp.
        $user_visit
          ->set('referer', $this->requestStack->getCurrentRequest()->headers->get('referer'))
          ->save();
      }
      else {
        UserVisit::create([
          'uid' => $user->id(),
          'vuid' => $this->account->id(),
          'referer' => $this->requestStack->getCurrentRequest()->headers->get('referer'),
        ])->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

}
