<?php

namespace Drupal\user_visits;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an user visit entity type.
 */
interface UserVisitInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
