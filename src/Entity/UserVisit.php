<?php

namespace Drupal\user_visits\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\user_visits\UserVisitInterface;

/**
 * Defines the user visit entity class.
 *
 * @ContentEntityType(
 *   id = "user_visit",
 *   label = @Translation("User visit"),
 *   label_collection = @Translation("User visits"),
 *   label_singular = @Translation("user visit"),
 *   label_plural = @Translation("user visits"),
 *   label_count = @PluralTranslation(
 *     singular = "@count user visits",
 *     plural = "@count user visits",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\user_visits\UserVisitListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\user_visits\Form\UserVisitForm",
 *       "edit" = "Drupal\user_visits\Form\UserVisitForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\user_visits\Routing\UserVisitHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "user_visit",
 *   admin_permission = "administer user visits",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/user-visit",
 *     "add-form" = "/admin/content/user-visit/add",
 *     "canonical" = "/admin/content/user-visit/{user_visit}",
 *     "edit-form" = "/admin/content/user-visit/{user_visit}",
 *     "delete-form" = "/admin/content/user-visit/{user_visit}/delete",
 *   },
 * )
 */
class UserVisit extends ContentEntityBase implements UserVisitInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * Get the visitor UID.
   *
   * @return int
   *   The visitor's uid.
   */
  public function getVuid() {
    return $this->get('vuid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Visitor'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['vuid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Visited'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Count'))
      ->setDescription(t('The count of the user visits.'));
    $fields['referer'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Referer'))
      ->setDescription(t('The referer url.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ]);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the user visit was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed on'))
      ->setDescription(t('The time that the user visit was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

}
